﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Wave
{
    public Dictionary<int, GameObject[]> spawnOrder;
    public int maxRandomGroupSize;
    public int minRandomGroupSize;
    public int maxEnemiesToSpawn;
    public int minEnemiesToSpawn;
    public int enemiesToSpawn;
    public float spawnIntervalBetweenSingles;
    public float spawnIntervalBetweenGroups;
    public float wavePreperationTime;
    public SpawnOption spawnOption;
    public Wave(int maxRanGroup, int minRanGroup, int maxEnemies, int minEnemies, float spawnInterval, float groupSpawnInterval, float prepTime, SpawnOption so, GameObject[] enemiesThatCanSpawn)
    {
        maxRandomGroupSize = maxRanGroup;
        minRandomGroupSize = minRanGroup;
        maxEnemiesToSpawn = maxEnemies;
        minEnemiesToSpawn = minEnemies;
        spawnIntervalBetweenGroups = groupSpawnInterval;
        spawnIntervalBetweenSingles = spawnInterval;
        wavePreperationTime = prepTime;
        spawnOption = so;

        spawnOrder = new Dictionary<int, GameObject[]>();

        enemiesToSpawn = Random.Range(minEnemiesToSpawn, maxEnemiesToSpawn);
        int enemiesLeftToAllocate = enemiesToSpawn;
        int spawnOrderCount = 0;
        //ArrayList enemiesForSpawnOrder = new ArrayList();
        while(enemiesLeftToAllocate > 0)
        {
            int groupSizeThisDraw = Random.Range(minRandomGroupSize, maxRandomGroupSize);
            if (groupSizeThisDraw > enemiesLeftToAllocate)
                groupSizeThisDraw = enemiesLeftToAllocate;
            GameObject[] enemiesForSpawnOrder = new GameObject[groupSizeThisDraw];
            for (int i = 0; i < groupSizeThisDraw; i++)
            {
                enemiesForSpawnOrder[i] = enemiesThatCanSpawn[Random.Range(0, enemiesThatCanSpawn.Length)];
            }
            spawnOrder.Add(spawnOrderCount, enemiesForSpawnOrder);
            spawnOrderCount++;
            enemiesLeftToAllocate -= groupSizeThisDraw;
        }
    }

    public Wave(Dictionary<int, GameObject[]> spawns)
    {
        spawnOrder = spawns;
    }
}
