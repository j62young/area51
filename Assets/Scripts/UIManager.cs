﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class UIManager : MonoBehaviour
{

    public Image healtBarImage;
    public Text scoreText;
    public Image[] placeableShieldIcons;
    // Start is called before the first frame update
    void Start()
    {
        NarutoRunner.onDamageTaken += SetHealthBar;
        GameManager.onScoreChange += SetScoreText;
        NarutoRunner.shieldCountUpdate += SetShieldIcons;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void SetShieldIcons(int shieldsReady)
    {

        foreach(Image i in placeableShieldIcons)
        {
            i.enabled = false;
            if(shieldsReady > 0)
            {
                i.enabled = true;
                shieldsReady--;
            }
        }
    }
    public void SetScoreText(float score)
    {
        scoreText.text = "Score: " + Mathf.FloorToInt(score);
    }
    public void SetHealthBar(GameObject player)
    {
        healtBarImage.fillAmount = player.GetComponent<NarutoRunner>().health / 125f;
    }
}
