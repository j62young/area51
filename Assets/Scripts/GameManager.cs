﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{

    public float playerScore = 0;
    public int aliensKilled = 0;
    public bool isGameOver = false;
    public bool isGamePaused = false;
    public Camera DeathCamera;
    public string gameOverScreen;
    public delegate void GameStateHandler<T>(T Value);

    public static event GameStateHandler<float> onScoreChange;
    // Start is called before the first frame update
    void Start()
    {
        Characters.onCharacterKilled += onKill;
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void onKill(GameObject dyingCharacter)
    {
        switch (dyingCharacter.tag)
        {
            case "Player":
                isGameOver = true;
                SceneManager.LoadScene(gameOverScreen, LoadSceneMode.Single);
                break;
            case "Aliens":
            default:
                aliensKilled++;
                playerScore += dyingCharacter.GetComponent<Alien>().scoreValue;
                onScoreChange?.Invoke(playerScore);
                break;
        }
    }


    
}
