﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    [SerializeField]
    public float projectileSpeed;
    [SerializeField]
    public float projectileDamage;
    [SerializeField]
    public float maxLifeTime;
    [SerializeField]
    public Vector2 forwardVector;
    public Animator animator;
    public bool initialized = false;
    public string owner;

    // Start is called before the first frame update
    void Start()
    {
        //gameObject.GetComponent<Renderer>().enabled = false;
        //setDirection();
        
        //Debug.Log("In Start");
    }

    // Update is called once per frame
    void Update()
    {
            moveBullet();


            //Garbage collection incase the bullett misses and goes off screen
            if (maxLifeTime < 0)
                removeBullet();
            maxLifeTime -= Time.deltaTime;
    }

    private void moveBullet()
    {
        transform.Translate(forwardVector * projectileSpeed * Time.deltaTime, Space.World);
    }

    public void SetForwardVector(Vector2 newForwardVector)
    {
        forwardVector = newForwardVector;
        if (forwardVector.x == 0 && forwardVector.y == 0)
            forwardVector.x = 1;
    }

    public void SetOwnerTag(string tag)
    {
        owner = tag;
    }

    void setDirection()
    {
        if(forwardVector.x == 1)
        { 
            if(forwardVector.y == 1)
            {
                animator.SetInteger("Direction", 1);
            } else if(forwardVector.y == 0)
            {
                animator.SetInteger("Direction", 0);
            } else
            {
                animator.SetInteger("Direction", 7);
            }
        }
        else if(forwardVector.x ==0)
        {
            if (forwardVector.y == 1)
            {
                animator.SetInteger("Direction", 2);
            }
            else if (forwardVector.y == 0)
            {
                //Center should never happen for bullet
            }
            else
            {
                animator.SetInteger("Direction", 6);
            }
        }
        else
        {
            if (forwardVector.y == 1)
            {
                animator.SetInteger("Direction", 3);
            }
            else if (forwardVector.y == 0)
            {
                animator.SetInteger("Direction", 4);
            }
            else
            {
                animator.SetInteger("Direction", 5);
            }
        }
    }
    public void removeBullet()
    {
        Destroy(gameObject);
    }

    //private void OnCollisionEnter2D(Collision2D collision)
    //{
    //    Debug.Log("Bullet Collision: " + collision.otherCollider);
    //}

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.CompareTag("Walls")){
            removeBullet();
        }
    }
}
