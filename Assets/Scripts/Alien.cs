﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Alien : Characters
{
    [Header("Alien Info")]
    public float pursuitDistance;
    public float stoppingDistance;
    public float retreatDistance;
    public float scoreValue;
    public Image healthBarImage;

    bool isTargetAlive = true;
    Transform player;
        
    // Start is called before the first frame update
    void Start()
    {
        Characters.onCharacterKilled += this.PlayerDied;
        player = GameObject.FindGameObjectWithTag("Player").transform;
        attackDelay = attackSpeed;
    }

    // Update is called once per frame
    void Update()
    {
        isFiring = false;
        if (isAlive && isTargetAlive)
        {
            if(damageIndicatorDelay > 0 && damageIndicatorFlashCount < damageIndicatorFlashAmount)
            {
                damageIndicatorDelay -= Time.deltaTime;
            } else if(damageIndicatorDelay <= 0 && damageIndicatorFlashCount < damageIndicatorFlashAmount)
            {
                IndicateDamage();
            }
            if (Vector2.Distance(transform.position, player.position) > stoppingDistance && Vector2.Distance(transform.position, player.position) < pursuitDistance)
            {
                transform.position = Vector2.MoveTowards(transform.position, player.position, speed * Time.deltaTime);
            }
            else if (Vector2.Distance(transform.position, player.position) < stoppingDistance && Vector2.Distance(transform.position, player.position) > retreatDistance)
            {
                transform.position = this.transform.position;
            }
            else if (Vector2.Distance(transform.position, player.position) < retreatDistance)
            {
                transform.position = Vector2.MoveTowards(transform.position, player.position, -speed * Time.deltaTime);
            }
            
            if (attackDelay <= 0 && player.gameObject.activeSelf)
            {
                if (Vector2.Distance(transform.position, player.position) < pursuitDistance)
                {
                    firingVector = new Vector2(player.position.x - transform.position.x, player.position.y - transform.position.y);

                    FireWeapon();
                }
            }
            else
            {
                attackDelay -= Time.deltaTime;
            }
            float playerHorizontalDirection = (transform.position.x - player.position.x) > 0 ? 1 : -1;
            float playerVerticalDirection = (player.position.y - transform.position.y) > 0 ? 1 : -1;
            AnimationHandler(playerHorizontalDirection,playerVerticalDirection);

        }
        else
        {
            //Do Dead things...
        }
    }
    public override void takeDamage(float damageTaken)
    {
        base.takeDamage(damageTaken);
        HandleHealthBar();
    }
    private void HandleHealthBar()
    {
        healthBarImage.fillAmount = health / 100;
    }
    public void PlayerDied(GameObject dyingObject)
    {
        if(dyingObject.tag == "Player")
            isTargetAlive = false;
    }
}
