﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnManager : MonoBehaviour
{

    public GameObject[] spawners;
    public GameObject[] enemiesToSpawn;
    [SerializeField]
    public Dictionary<int, Wave> Waves;
    public int currentWave;
    public int enemiesLeftInWave;
    public float timeToNextWave;
    public float timeToNextSpawn;
    int currentSpawnOrder = 0;
    bool isPlayerAlive = true;
    // Start is called before the first frame update
    void Start()
    {
        Characters.onCharacterKilled += this.PlayerDied;
        Waves = new Dictionary<int, Wave>();
        CreateWaves(50);
        timeToNextWave = Waves[0].wavePreperationTime;
    }

    // Update is called once per frame
    void Update()
    {
        if (timeToNextWave <= 0 && isPlayerAlive)
        {
            //Start Spawning the wave;
            if (timeToNextSpawn <= 0)
            {
                Spawn();
            }
            else
            {
                timeToNextSpawn -= Time.deltaTime;
            }
        }
        else
        {
            timeToNextWave -= Time.deltaTime;
        }
    }

    public void Spawn()
    {
        Wave waveToSpawn = Waves[currentWave];
        Transform spawnerToSpawnAt;
        if (waveToSpawn.spawnOrder.Count > 1)
        {
            spawnerToSpawnAt = spawners[Random.Range(0, spawners.Length)].transform;
            Instantiate(waveToSpawn.spawnOrder[currentSpawnOrder][0], spawnerToSpawnAt.position, Quaternion.identity);
            timeToNextSpawn = waveToSpawn.spawnIntervalBetweenSingles;
        }
        else
        {
            spawnerToSpawnAt = spawners[Random.Range(0, spawners.Length)].transform;
            foreach (GameObject o in waveToSpawn.spawnOrder[currentSpawnOrder])
            {
                Instantiate(o, spawnerToSpawnAt.position, Quaternion.identity);
            }
            timeToNextSpawn = waveToSpawn.spawnIntervalBetweenGroups;
        }
        
        currentSpawnOrder++;
        if(currentSpawnOrder == waveToSpawn.spawnOrder.Count)
        {
            timeToNextWave = Waves[++currentWave].wavePreperationTime;
            timeToNextSpawn = 0;
            currentSpawnOrder = 0;
            enemiesLeftInWave = Waves[currentWave].enemiesToSpawn;
        }

    }

    private void CreateWaves(int numberToCreate)
    {
        Wave newWave;
        int minRandomGroupSize = Random.Range(1,4);
        int maxRandomGroupSize = Random.Range(minRandomGroupSize,6);

        int minEnemiesToSpawn = Random.Range(10,16);
        int maxEnemiesToSpawn = Random.Range(minEnemiesToSpawn, 26);


        float spawnIntervalBetweenSingles = Random.Range(0.2f,1.5f);
        float spawnIntervalBetweenGroups = Random.Range(1f,2f);
        float wavePreperationTime = Random.Range(5f,10f);

        for(int i = 0; i < numberToCreate; i++)
        {
            newWave = new Wave(maxRandomGroupSize, minRandomGroupSize, maxEnemiesToSpawn, minEnemiesToSpawn, spawnIntervalBetweenSingles, spawnIntervalBetweenGroups, wavePreperationTime, SpawnOption.Single, enemiesToSpawn);
            Waves.Add(i,newWave);
        }
    }

    public void PlayerDied(GameObject dyingObject)
    {
        if (dyingObject.tag == "Player")
            isPlayerAlive = false;
    }
}

public enum SpawnOption
{
    Single = 0,
    Squad,
    Random,
    RandomSquad,
    FireGoo,
    IceGoo,
    GreyGoo,
    Boss
}