﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shield : MonoBehaviour
{
    [SerializeField]
    public float health = 100;
    public string owner;

    public delegate void ShieldEventHandler<T>(T Values);

    public static event ShieldEventHandler<string> onShieldDestroy;

    protected void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Bullets"))
        {
            Bullet bulletScript = collision.gameObject.GetComponent<Bullet>();
            if (bulletScript.owner != owner) {
                takeDamage(bulletScript.projectileDamage);
                bulletScript.removeBullet();
            }
        }
    }

    public void SetOwner(string newOwner)
    {
        owner = newOwner;
    }

    public void takeDamage(float damageTaken)
    {
        health -= damageTaken;
        if (health <= 0)
        {
            removeShield();
        }
    }

    public void removeShield() {
        onShieldDestroy?.Invoke(owner); //Fire Delegate
        Destroy(gameObject);
    }
}
