﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NarutoRunner : Characters
{

    public Dictionary<string, int> ammo;
    public int shieldCount = 3;
    public GameObject shieldToDeploy;
    public Camera characterCamera;

    public static event CharacterEventHandler<GameObject> onDamageTaken;
    public static event CharacterEventHandler<int> shieldCountUpdate;
    // Start is called before the first frame update
    void Start()
    {
        if (characterCamera == null)
            characterCamera = Camera.main;
        Shield.onShieldDestroy += this.recoverShield;
        
    }
    
    // Update is called once per frame
    void Update()
    {
        isFiring = false;
        if (isAlive)
        {
            CalculateForwardVector();
            CalculateFiringVector();
            MoveCharacter();

            if (damageIndicatorDelay > 0 && damageIndicatorFlashCount < damageIndicatorFlashAmount)
            {
                damageIndicatorDelay -= Time.deltaTime;
            }
            else if (damageIndicatorDelay <= 0 && damageIndicatorFlashCount < damageIndicatorFlashAmount)
            {
                IndicateDamage();
            }

            if (attackDelay <=0 && Input.GetAxis("Fire1") > 0)
            {

                FireWeapon();
            }else if((Input.GetAxis("Fire2") > 0) && attackDelay <=0)
            {
                deployShield();
            }
            if (attackDelay > 0)
                attackDelay -= Time.deltaTime;
            AnimationHandler(Input.GetAxisRaw("Horizontal"), Input.GetAxisRaw("Vertical"));
        }
        else
        {
            //Do Dead Things...
        }
    }

    private void CalculateForwardVector()
    {
        forwardVector.Set(Input.GetAxisRaw("Horizontal"), Input.GetAxisRaw("Vertical"));
        //if (forwardVector.x != 0 || forwardVector.y != 0)
           // firingVector = forwardVector;
    }

    private void CalculateFiringVector()
    {

        Vector2 positionInWorld = characterCamera.ScreenToWorldPoint(Input.mousePosition);
        firingVector = new Vector2(positionInWorld.x - transform.position.x,positionInWorld.y - transform.position.y);
    }
    private void MoveCharacter()
    {
        this.transform.Translate(forwardVector * speed * Time.deltaTime, Space.Self);
        characterCamera.transform.position = new Vector3(transform.position.x, transform.position.y, characterCamera.transform.position.z);
    }  

    public void deployShield()
    {
        if (attackDelay <= 0)
        {
            if (shieldToDeploy != null && shieldCount > 0)
            {
                
                Debug.Log(shieldCount);
                GameObject newShieldObject = Instantiate(shieldToDeploy, new Vector3(this.transform.position.x, this.transform.position.y, 1), Quaternion.identity);
                newShieldObject.SendMessage("SetOwner", tag);
                isFiring = true;
                attackDelay = attackSpeed;
                shieldCount-=1;
                shieldCountUpdate?.Invoke(shieldCount);
            }
        }
    }

    public void recoverShield(string destroyedShieldOwner)
    {
        shieldCount++;
        shieldCountUpdate?.Invoke(shieldCount);
    }

    public override void takeDamage(float damageTaken)
    {

        health -= damageTaken;
        damageIndicatorDelay = damageIndicatorTime;
        damageIndicatorFlashCount = 0;
        onDamageTaken?.Invoke(gameObject);
        if (health <= 0)
        {
            OnKilled();
        }
    }

}
