﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Characters : MonoBehaviour
{
    [Header("Stats")]
    public float health;
    public float speed;
    public float attackSpeed;
    public float attackDelay;
    public Vector2 forwardVector;
    public Vector2 firingVector;
    public bool isFiring = false;
    public bool isAlive = true;
    public GameObject bulletToFire;
    public Animator animator;
    public SpriteRenderer spriteRenderer;

    public float damageIndicatorTime;
    public float damageIndicatorDelay;
    public Color damageIndicatorColor;
    public int damageIndicatorFlashCount;
    public int damageIndicatorFlashAmount;
    public delegate void CharacterEventHandler<T>(T Value);

    public static event CharacterEventHandler<GameObject> onCharacterKilled;
    // Start is called before the first frame update
    void Start()
    {
        attackDelay = attackSpeed;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    //private void OnCollisionEnter2D(Collision2D collision)
    //{
    //    Debug.Log(collision.otherCollider);
    //}

    protected void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Bullets"))
        {
            Bullet bulletScript = collision.gameObject.GetComponent<Bullet>();
            if (bulletScript.owner != gameObject.tag)
            {
                takeDamage(bulletScript.projectileDamage);
                bulletScript.removeBullet();
            }
        }
    }

    public virtual void takeDamage(float damageTaken)
    {
        health -= damageTaken;
        damageIndicatorDelay = damageIndicatorTime;
        damageIndicatorFlashCount = 0;
        if (health <= 0)
        {
            OnKilled();
        }
    }

    public virtual void IndicateDamage()
    {
        
        if(spriteRenderer.color == damageIndicatorColor)
        {
            spriteRenderer.color = Color.white;
            damageIndicatorFlashCount++;
        } else
        {
            spriteRenderer.color = damageIndicatorColor;
        }
        

        if (damageIndicatorFlashCount < damageIndicatorFlashAmount)
            damageIndicatorDelay = damageIndicatorTime;

    }
    protected void FireWeapon()
    {
        if (bulletToFire != null)
        {
            GameObject newBulletObject = Instantiate(bulletToFire, new Vector3(transform.position.x, transform.position.y, 1), Quaternion.identity);
            firingVector.Normalize(); //So Projectile doesn't go faster the farther away you are..(could be fun though)
            newBulletObject.GetComponent<Bullet>().SendMessage("SetForwardVector", firingVector);
            newBulletObject.SendMessage("SetOwnerTag", gameObject.tag);
            isFiring = true;

        }
        attackDelay = attackSpeed;
    }
    protected void AnimationHandler(float horizontalMovement, float verticalMovement)
    {
        animator.SetFloat("Horizontal", horizontalMovement);
        animator.SetFloat("Vertical", verticalMovement);
        animator.SetBool("Firing", isFiring);
    }

    public virtual void OnKilled()
    {
        onCharacterKilled?.Invoke(this.gameObject);
        isAlive = false;
        Destroy(gameObject);
    }
}
