﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuController : MonoBehaviour
{
    public string nextScene;
    // Update is called once per frame
  //  void Update()
  //  {
  //      if (Input.GetAxis("Fire1") > 0) {
  //          GoToNextScene();
		//}

  //      if (Input.GetAxis("Fire2") > 0) {
  //          QuitGame();

  //      }
  //  }

    public void GoToNextScene()
    {
        SceneManager.LoadScene(nextScene, LoadSceneMode.Single);
    }

    public void QuitGame()
    {
        Debug.Log("Quiting Game");
        Application.Quit();
        
    }
}
